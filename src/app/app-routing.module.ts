import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./components/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'create-agent',
    loadChildren: () => import('./components/agent/create-agent/create-agent.module').then( m => m.CreateAgentPageModule)
  },
  {
    path: 'history-agent',
    loadChildren: () => import('./components/agent/history-agent/history-agent.module').then( m => m.HistoryAgentPageModule)
  },
  {
    path: 'create-track',
    loadChildren: () => import('./components/track/create-track/create-track.module').then( m => m.CreateTrackPageModule)
  },
  {
    path: 'history-track',
    loadChildren: () => import('./components/track/history-track/history-track.module').then( m => m.HistoryTrackPageModule)
  },
  {
    path: 'tab4',
    loadChildren: () => import('./components/tab4/tab4.module').then( m => m.Tab4PageModule)
  },
  {
    path: 'create-secretary',
    loadChildren: () => import('./components/secretary/create-secretary/create-secretary.module').then( m => m.CreateSecretaryPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
