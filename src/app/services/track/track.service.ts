import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Track } from 'src/app/models/Track';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TrackService {

  urlTracks = environment.apiURL + '/track';

  constructor(private http: HttpClient) { }

  getAllAllowedTracks(): Observable<Track[]> {
    return this.http.get<Track[]>(this.urlTracks + '/levelCongestionGreaterThan/30');
  }

  getAllTracks(): Observable<Track[]> {
    return this.http.get<Track[]>(this.urlTracks + '/tracks');
  }

  getTrack(trackId: number): Observable<Track> {
    return this.http.get<Track>(this.urlTracks + '/' + trackId);
  }

  createTrack(track: Track): Observable<Track> {
    return this.http.post<Track>(this.urlTracks + '/create-track', track);
  }

  editTrack(track: Track): Observable<Track> {
    return this.http.put<Track>(this.urlTracks + '/edit-track', track);
  }

  deleteTrack(trackId: number): Observable<object> {
    return this.http.delete(this.urlTracks + '/delete-track/' + trackId);
  }

}
