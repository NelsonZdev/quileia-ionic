import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HistoryAssignation } from 'src/app/models/HistoryAssignation';
import { HistoryService } from 'src/app/services/history/history.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{

  dbHistory: Observable<HistoryAssignation[]>;
  @ViewChild('searchBar') searchBar: IonSearchbar;
  showSearch = false;
  textSearch = '';

  constructor(
    private historyService: HistoryService
  ) {}

  ngOnInit(): void {
    this.dbHistory = this.historyService.getAllHistory();
  }

  showSearchbar(): void {
    this.showSearch = true;
    setTimeout(() => {
      this.searchBar.setFocus();
    }, 500);
  }

  hideSearchbar(): void {
    this.showSearch = false;
  }

  searchHistory(event: any): void  {
    this.textSearch = event.detail.value;
  }

}
