import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoryTrackPageRoutingModule } from './history-track-routing.module';

import { HistoryTrackPage } from './history-track.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoryTrackPageRoutingModule
  ],
  declarations: [HistoryTrackPage]
})
export class HistoryTrackPageModule {}
