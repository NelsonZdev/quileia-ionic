import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HistoryAssignation } from 'src/app/models/HistoryAssignation';
import { Track } from 'src/app/models/Track';
import { HistoryService } from 'src/app/services/history/history.service';

@Component({
  selector: 'app-history-track',
  templateUrl: './history-track.page.html',
  styleUrls: ['./history-track.page.scss'],
})
export class HistoryTrackPage implements OnInit {

  dbHistory: Observable<HistoryAssignation[]>;
  dbTrack: Track;

  constructor(
    private router: Router,
    private historyService: HistoryService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    if (this.activatedRoute.snapshot.params.track !== undefined) {
      this.dbTrack = JSON.parse(this.activatedRoute.snapshot.params.track);
      this.dbHistory = this.historyService.getHistoryOfTrack(this.dbTrack.id);
    }
  }

}
