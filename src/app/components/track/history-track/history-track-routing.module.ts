import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoryTrackPage } from './history-track.page';

const routes: Routes = [
  {
    path: '',
    component: HistoryTrackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoryTrackPageRoutingModule {}
