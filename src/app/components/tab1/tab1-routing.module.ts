import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateAgentPage } from '../agent/create-agent/create-agent.page';
import { HistoryAgentPage } from '../agent/history-agent/history-agent.page';
import { Tab1Page } from './tab1.page';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
  },
  {
    path: 'create-agent',
    component: CreateAgentPage
  },
  {
    path: 'history-agent',
    component: HistoryAgentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule {}
