import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateSecretaryPage } from './create-secretary.page';

const routes: Routes = [
  {
    path: '',
    component: CreateSecretaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateSecretaryPageRoutingModule {}
