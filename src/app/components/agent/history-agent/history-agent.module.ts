import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoryAgentPageRoutingModule } from './history-agent-routing.module';

import { HistoryAgentPage } from './history-agent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoryAgentPageRoutingModule
  ],
  declarations: [HistoryAgentPage]
})
export class HistoryAgentPageModule {}
