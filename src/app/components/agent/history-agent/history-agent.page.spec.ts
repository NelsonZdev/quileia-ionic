import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HistoryAgentPage } from './history-agent.page';

describe('HistoryAgentPage', () => {
  let component: HistoryAgentPage;
  let fixture: ComponentFixture<HistoryAgentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryAgentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HistoryAgentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
