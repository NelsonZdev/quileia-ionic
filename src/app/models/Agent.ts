export interface Agent {
    id: number;
    nameA: string;
    lastNameA: string;
    yearsExperienceA: number;
    trackIdA: number;
    secretaryIdA: number;
}
