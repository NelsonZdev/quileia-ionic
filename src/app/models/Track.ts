export interface Track {
    id: number;
    typeT: string;
    streetOrCarrerT: string;
    numberT: string;
    levelCongestionT: DoubleRange;
}
