import { Pipe, PipeTransform } from '@angular/core';
import { Track } from '../models/Track';

@Pipe({
  name: 'filterTrack'
})
export class FilterTrackPipe implements PipeTransform {

  transform(tracks: Track[], text: string): Track[] {

    if (text.length === 0) {
      return tracks;
    }

    text = text.toLowerCase();

    if (tracks !== null) {
      return tracks.filter((track) => {
        return track.streetOrCarrerT.toLowerCase().includes(text)
        || track.id.toString() === text
        || track.numberT.toString() === text
        || track.levelCongestionT.toString() === text
        || track.numberT.toString() === text
        || track.typeT.toLowerCase().includes(text);
      });
    }

    return tracks;

  }

}
