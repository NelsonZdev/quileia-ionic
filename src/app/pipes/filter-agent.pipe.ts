import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { Agent } from '../models/Agent';
import { filter } from 'rxjs/operators';

@Pipe({
  name: 'filterAgent'
})
export class FilterAgentPipe implements PipeTransform {

  transform(agents: Agent[], text: string): Agent[] {

    if (text.length === 0) {
      return agents;
    }

    text = text.toLowerCase();

    if (agents !== null) {
      return agents.filter((agent) => {
        return agent.nameA.toLowerCase().includes(text) || agent.id.toString() === text;
      });
    }

    return agents;

  }

}
