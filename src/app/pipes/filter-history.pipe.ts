import { Pipe, PipeTransform } from '@angular/core';
import { HistoryAssignation } from '../models/HistoryAssignation';

@Pipe({
  name: 'filterHistory'
})
export class FilterHistoryPipe implements PipeTransform {

  transform(historys: HistoryAssignation[], text: string): HistoryAssignation[] {

    if (text.length === 0) {
      return historys;
    }

    text = text.toLowerCase();

    if (historys !== null) {
      return historys.filter((history) => {
        return history.id.toString() === text
        || history.agentNameH.toLowerCase().includes(text)
        || history.trackNameH.toString() === text
        || history.agentIdH.toString() === text
        || history.trackIdH.toString() === text
        || history.startDateH.toString() === text;
      });
    }

    return historys;

  }

}
