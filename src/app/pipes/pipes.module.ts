import { NgModule } from '@angular/core';
import { FilterAgentPipe } from './filter-agent.pipe';
import { FilterTrackPipe } from './filter-track.pipe';
import { FilterHistoryPipe } from './filter-history.pipe';
import { FilterSecretaryPipe } from './filter-secretary.pipe';



@NgModule({
  declarations: [FilterAgentPipe, FilterTrackPipe, FilterHistoryPipe, FilterSecretaryPipe],
  exports: [ FilterAgentPipe, FilterTrackPipe, FilterHistoryPipe, FilterSecretaryPipe ]
})
export class PipesModule { }
